Imports System.Console
Module Module1

    Sub Main()
        Dim n As Integer
        Write("Enter any Number: ")
        n = ReadLine()
        If n < 0 Then
            Write("It is an NEGATIVE number")
        ElseIf n > 0 Then
            Write("It is a POSITIVE number")
        Else
            Write("It is ZERO")
        End If
        ReadLine()
    End Sub

End Module