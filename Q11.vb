Imports System.Console
Module module1
    Sub Main()
        Dim ch As Integer
        WriteLine("Which Program you want to run Question 5/6/7")
        Write("Enter your choise :(5/6/7) ")
        ch = Readline()
        Select Case ch
            Case 5
                Call Q5()
            Case 6
                Call Q6()
            Case 7
                Call Q7()
            Case Else
                Write("Not valid")

        End Select
    End Sub
    Sub Q5()
        Dim Q, P, R, N As Integer
        WriteLine("This Program Will Find The Compound Interest")
        WriteLine()
        Write("Enter value for (P) : ")
        P = ReadLine()
        Write("Enter value for (R) : ")
        R = ReadLine()
        Write("Enter value for (N) : ")
        N = ReadLine()
        Q = P * 1 + (R / N) ^ N
        WriteLine()
        WriteLine("Compound Interest for P = " & P & "  R = " & R & "  N = " & N & "  is = " & Q)
        ReadLine()
    End Sub

    Sub Q6()
        Dim x, n, i, j As Integer
        WriteLine("This Program Will find out the value of X")
        Write("Enter Value For X : ")
        x = ReadLine()
        Write("Enter Value For n : ")
        n = ReadLine()
        For i = 0 To n
            j = x * x
        Next i
        WriteLine("Ans= " & j)
        ReadLine()
    End Sub
    Sub Q7()
        Dim n, s(7), f, ill, i, subj As Integer
        Dim na(50) As Char
        Dim per, tot As Single

        f = 0
        ill = 0
        Write("Enter no     : ")
        n = ReadLine()
        Write("Enter name   : ")
        na = ReadLine()
        Write("Enter no of subject     : ")
        subj = ReadLine()

        For i = 0 To (subj - 1)
            WriteLine()
            WriteLine()
            Write("Enter marks of 30" & i + 1 & " ")
            s(i) = ReadLine()
            If s(i) < 40 Then
                f = 1
            End If
            If s(i) > 100 Then
                ill = 1
            End If
        Next i
        WriteLine("             RESULT:")
        WriteLine()
        WriteLine("     Roll No     :" & n)
        WriteLine()
        WriteLine("     Name        :" & na)
        If ill = 1 Then
            WriteLine("You enter marks more then 100 (not valid) ")
        Else

            tot = 0
            For i = 0 To (subj - 1)

                WriteLine("     Enter marks of 30" & i + 1 & " " & s(i))
                tot = tot + s(i)
            Next i

            per = tot / subj
            WriteLine("     Total = " & tot)
            WriteLine("     Percentage = " & per & "%")
            Write("     Grad=")
            If f = 0 Then

                If (per > 75) Then
                    Write("Distinction")
                ElseIf (per >= 60 And per < 75) Then
                    Write("First class")
                ElseIf (per >= 50 And per < 60) Then
                    Write("Second class")
                ElseIf (per >= 40 And per < 50) Then
                    Write("Pass")
                End If
            ElseIf (per < 40 Or f = 1) Then
                Write("Fail")
            End If
        End If
        ReadLine()
        'Programming by Himakshiba P. Gohil
    End Sub
End Module
