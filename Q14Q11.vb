Imports System.Console
Module Module1

    Sub Main()
        Dim i, j, k As Integer
        For i = 1 To 4
            For k = 6 To i Step -1
                Write(" ")
            Next k
            For j = 1 To i
                Write(j * j)
            Next j
            WriteLine()
        Next i
        ReadLine()
    End Sub

End Module
