Imports System.Console
Module Module1
    Sub Main()
        Dim i, j, k, s As Integer
        For i = 1 To 5
            WriteLine()
        Next i
        For j = 1 To 5 Step +2
            For s = 15 To j Step -1
                Write(" ")
            Next s
            For i = 1 To j
                Write(i)
                Write(" ")
            Next i
            For k = i - 2 To 1 Step -1
                Write(" ")
            Next k
            WriteLine()
        Next j
        ReadLine()
    End Sub
End Module
'        1
'    2   3   2
'3   4   5   4   3