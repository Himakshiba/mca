Imports System.console
Module Module1

    Sub Main()
        Dim I, P, R, N As Integer
        WriteLine("This Program Will Find The Simple Interest")
        WriteLine()
        Write("Enter value for (P) : ")
        P = ReadLine()
        Write("Enter value for (R) : ")
        R = ReadLine()
        Write("Enter value for (N) : ")
        N = ReadLine()
        I = (P * R * N) / 100
        WriteLine()
        WriteLine("Simple Interest for P = " & P & "  R = " & R & "  N = " & N & "  is = " & I)
        ReadLine()
    End Sub

End Module
