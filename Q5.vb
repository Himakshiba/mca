Imports System.console
Module Module1

    Sub Main()
        Dim Q, P, R, N As Integer
        WriteLine("This Program Will Find The Compound Interest")
        WriteLine()
        Write("Enter value for (P) : ")
        P = ReadLine()
        Write("Enter value for (R) : ")
        R = ReadLine()
        Write("Enter value for (N) : ")
        N = ReadLine()
        Q = P * 1 + (R / N) ^ N
        WriteLine()
        WriteLine("Simple Interest for P = " & P & "  R = " & R & "  N = " & N & "  is = " & Q)
        ReadLine()
    End Sub

End Module
