Imports System.console
Module module1
    Sub main()
        Dim n, s(7), f, ill, i, subj As Integer
        Dim na(50) As Char
        Dim per, tot As Single

        f = 0
        ill = 0
        Write("Enter no     : ")
        n = ReadLine()
        Write("Enter name   : ")
        na = ReadLine()
        Write("Enter no of subject     : ")
        subj = ReadLine()

        For i = 0 To (subj - 1)
            WriteLine()
            WriteLine()
            Write("Enter marks of 30" & i + 1 & " ")
            s(i) = ReadLine()
            If s(i) < 40 Then
                f = 1
            End If
            If s(i) > 100 Then
                ill = 1
            End If
        Next i

        Write("             RESULT:")
        WriteLine()
        WriteLine()
        Write("     Roll No     :" & n)
        WriteLine()
        WriteLine()
        Write("     Name        :" & na)
        WriteLine()
        If ill = 1 Then
            WriteLine("You enter marks more then 100 (not valid) ")
        Else

            tot = 0
            For i = 0 To (subj - 1)

                WriteLine("     Enter marks of 30" & i + 1 & " " & s(i))
                tot = tot + s(i)
            Next i

            per = tot / subj
            WriteLine("     Total = " & tot)
            WriteLine("     Percentage = " & per & "%")
            Write("     Grad=")
            If f = 0 Then

                If (per > 75) Then
                    Write("Distinction")
                ElseIf (per >= 60 And per < 75) Then
                    Write("First class")
                ElseIf (per >= 50 And per < 60) Then
                    Write("Second class")
                ElseIf (per >= 40 And per < 50) Then
                    Write("Pass")
                End If
            ElseIf (per < 40 Or f = 1) Then
                Write("Fail")
            End If
        End If
        ReadLine()
        'Programming by Himakshiba P. Gohil
    End Sub
End Module