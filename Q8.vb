Imports system.console
Module Module1

    Sub Main()
        Dim y As Integer
        Write("Enter any number of year :")
        y = ReadLine()
        If (y Mod 4 = 0) Or (y Mod 100 = 0) Then
            Write("It is a leap year")
        Else
            Write("It is not a leap year")
        End If
        ReadLine()
    End Sub

End Module
