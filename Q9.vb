Imports System.Console
Module Module1

    Sub Main()
        Dim n As Integer
        Write("Enter any Number: ")
        n = ReadLine()
        If n Mod 2 = 0 Then
            Write("It is an EVEN number")
        Else
            Write("It is an ODD number")
        End If
        ReadLine()
    End Sub

End Module
