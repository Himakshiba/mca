Imports system.console
Module Module1

    Sub Main()
        Dim i, y As Integer
        Write("Enter stating year :")
        i = ReadLine()
        Write("Enter ending year :")
        y = ReadLine()
        WriteLine()
        WriteLine("Leap years between" & i & "to" & y & "are:")
        While (i < y)
            If (i Mod 4 = 0) Or (i Mod 100 = 0) Then
                WriteLine(i)
            End If
            i = i + 1
        End While
        ReadLine()
    End Sub

End Module
